<?php
/**
 * Created by PhpStorm.
 * User: dmitry.korol
 * Date: 24-Oct-19
 * Time: 17:23
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entity_id', 'score', 'keyword'
    ];

    // каждая картинка имеет уникальный hash
    // храним картинки в бд под этим уникальным hash-om, чтобы не было конфликтов
    // в поле keyword заносим все уникальные ключевики, которые получили в результате прогона
}