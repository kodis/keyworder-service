<?php
/**
 * Created by PhpStorm.
 * User: dmitry.korol
 * Date: 20-Jan-20
 * Time: 13:49
 */

namespace App\FtpUploaders;


use App\Services\KeyworderService;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class FtpStorage
{
    public function uploadImagesToMyFtp() : void
    {
        $this->uploadFilesToMyFTP('fotomaiki_ftp');
    }

    public function uploadFilesToMyFTP($myFtpServerCreds): void
    {
        try {
            $ftpName = config('filesystems.disks.'.$myFtpServerCreds)['name'];

            $keywordService = new KeyworderService();

            $files = $keywordService->getFilesNamesInFolder();

            foreach ($files as $file) {

                $path = public_path(config('app.constants.folder_origins').$file);
                $contents = File::get($path);
                try {
                    Storage::disk($myFtpServerCreds)->put(config('filesystems.disks.fotomaiki_ftp')['path'].$file, $contents);
                } catch (\Exception $e) {
                    dump('Uploader Error '.$ftpName, $e->getMessage());
                }
            }

            dump('Success uploaded to '.$ftpName);

        } catch (\Exception $e) {
            dump('Uploader Error', $e->getMessage());
        }
    }
}