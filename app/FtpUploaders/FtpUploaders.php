<?php
/**
 * Created by PhpStorm.
 * User: dmitry.korol
 * Date: 16-Nov-19
 * Time: 16:45
 */

namespace App\FtpUploaders;

use App\Services\KeyworderService;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class FtpUploaders
{
    public function uploadImagesToStocks() : void
    {
//        $this->uploadFilesToStock('shutterstock_ftp');
        $this->uploadFilesToStock('deposit_photos_ftp');
        $this->uploadFilesToStock('big_stock_photo_ftp');
        $this->uploadFilesToStock('123rf_ftp');
        $this->uploadFilesToStock('adobestock_ftp');
        $this->uploadFilesToStock('dreamstime_ftp');
    }

    /**
     * @param $photostockFtpCreds
     */
    public function uploadFilesToStock($photostockFtpCreds) : void
    {
        try {
            $stockName = config('filesystems.disks.'.$photostockFtpCreds)['name'];

            $keywordService = new KeyworderService();
            $files = $keywordService->getFilesNamesInFolder();

            foreach ($files as $file) {

                $path = public_path(config('app.constants.folder_done') . $file);
                $localFile = File::get($path);
                try {
                    Storage::disk($photostockFtpCreds)->put($file, $localFile);
                } catch (\Exception $e) {
                    dump('Uploader Error '. $stockName, $e->getMessage());
                }
            }

            dump('Success uploaded to '. $stockName);

        } catch (\Exception $e) {
            dump('Uploader DepositPhotos Error', $e->getMessage());
        }
    }
}