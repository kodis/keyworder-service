<?php

namespace App\Http\Controllers;

use App\Services\KeyworderService;

class KeywordsController
{
    public function saveAttributesToImages() : void
    {
        try {
            $keyworderService = new KeyworderService();
            $keyworderService->makeCopyOfImages();
            $files = $keyworderService->getFilesNamesInFolder();
            $keyworderService->uploadFilesToFtp();

            foreach ($files as $file) {
                $description = $keyworderService->getAdditionalTitle($file);
                $path = public_path(config('app.constants.folder_done') . $file);
                $title = $keyworderService->getTitle($file);
                $keywords = $keyworderService->getMergedUniqueKeywords($file);
                $description = $title.', '.$description;

                // Set the IPTC tags
                $iptcTags = [
                    '2#025' => $keywords, // keywords (keywords is array of keywords)
                    '2#080' => ['Dmitry Korol'], // author
                    '2#120' => [$description], // description of image
                    '2#085' => [$title], // author title
                    '2#005' => [$title], // custom title
                ];

                // Convert the IPTC tags into binary code
                $data = '';
                foreach ($iptcTags as $tag => $keys) {
                    $tag = substr($tag, 2);
                    foreach ($keys as $key) {
                        $data .= $keyworderService->generateImageEntity(2, $tag, $key);
                        // Embed the IPTC data
                        $content = iptcembed($data, $path);
                        // Write the new image data out to the file.
                        $fp = fopen($path, 'wb');
                        fwrite($fp, $content);
                        fclose($fp);
                    }
                }
            }
            dump('Finished');
        } catch (\Exception $e) {
            dump($e->getMessage());
        }
    }
}