<?php

use \Illuminate\Support\Arr;

if (!function_exists('getExceptionErrors')) {

    /**
     * @param Exception $e
     * @return array
     */
    function getExceptionErrors(\Exception $e): array
    {
        return dump([
            'error' => $e->getMessage(),
            'first' => Arr::get($e->getTrace(), 0),
            'second' => Arr::get($e->getTrace(), 1),
        ]);
    }
}