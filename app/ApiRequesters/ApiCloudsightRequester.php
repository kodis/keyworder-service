<?php

namespace App\ApiRequesters;

use \Exception;
use Illuminate\Support\Arr;

class ApiCloudsightRequester
{
    /**
     * @param $file
     * @return string
     */
    public function getTitleFromCloudsightService($file) : string
    {
        try {
            $title = $this->getCloudsightTitle(
                config('services.cloudSight.api_key'),
                public_path(config('app.constants.folder_origins') . $file));

            return $title;
        } catch (\Exception $e) {

            return getExceptionErrors($e);
        }
    }

    /**
     * @param $key
     * @param $file_url
     * @return string
     */
    public function getCloudsightTitle($key, $file_url)
    {
        try {
            $parse = parse_url($file_url);
            $size = getimagesize($file_url);
            $boundary = md5(time());

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, "https://api.cloudsightapi.com/image_requests");

            if (isset($parse['host'])) {
                $headers = array('Authorization: CloudSight '.$key, "Content-Type:multipart/form-data");

                $postFields = array(
                    'image_request' => array(
                        'remote_image_url'  => $file_url,
                        'locale' => 'en-US'
                    )
                );

                $body = http_build_query($postFields);
            } else {
                $headers = array('Authorization: CloudSight '.$key, "Content-Type:multipart/form-data; boundary=".$boundary);

                $body  = '';
                $body .= '--'.$boundary."\r\n";
                $body .= 'Content-Disposition: form-data; name="image_request[locale]"'."\r\n\r\n";
                $body .= "en-US"."\r\n";
                $body .= '--'.$boundary."\r\n";
                $body .= 'Content-Disposition: form-data; name="image_request[image]"; filename="'.basename($file_url).'"'."\r\n";
                $body .= 'Content-Type: '.$size['mime']."\r\n";
                $body .= 'Content-Transfer-Encoding: multipart/form-data'."\r\n\r\n";
                $body .= file_get_contents($file_url)."\r\n";
                $body .= '--'.$boundary .'--'."\r\n\r\n";

            }

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

            $ret = curl_exec($ch);
            $json = json_decode($ret, true);

            $token = $json['token'];
            $status = $json['status'];

            while ($status == 'not completed') {

                //grab the result
                curl_setopt($ch, CURLOPT_POST, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, null);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_URL, 'https://api.cloudsightapi.com/image_responses/'.$token);
                $ret = curl_exec($ch);

                $json = json_decode($ret, true);

                $token = $json['token'];
                $status = $json['status'];

                sleep(1);
            }

            $label = $json['name'];
            curl_close($ch);

            return $label;
        } catch (Exception $e) {
            dump('getCloudsightTitle() Exception', $e->getMessage());
        }
    }

    public function getExceptionErrors(\Exception $e) : array
    {
        return dump([
            'error' => $e->getMessage(),
            'first' => Arr::get($e->getTrace(), 0),
            'second' => Arr::get($e->getTrace(), 1),
        ]);
    }
}