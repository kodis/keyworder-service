<?php

namespace App\ApiRequesters;

use Illuminate\Support\Arr;
use PhpFanatic\clarifAI\ImageClient;


class ApiClarifaiRequester
{

    /**
     * @param $file
     * @return array
     */
    public function getClarifaiServiceResult($file) : array
    {
        try {
            $file = public_path(config('app.constants.folder_done') . $file);
            $base64 = file_get_contents($file);

            $client = new ImageClient(config('services.clarifai.api_key'));
            $client->AddImage($base64);

            $result =  json_decode($client->Predict(), true);
            $outputs = Arr::first($result['outputs'])['data']['concepts'];

            $keywords = [];

            foreach ($outputs as $output) {
                $keywords[] = $output['name'];
            }

            return $keywords;

        } catch (\Exception $e) {
            dump('Got ApiClarifaiRequester Error', $e->getMessage());
        }
    }
}