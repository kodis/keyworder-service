<?php

namespace App\ApiRequesters;


use App\Http\Controllers\KeywordsController;
use App\Services\KeyworderService;
use Vision\Feature;
use Vision\Request\Image\LocalImage;
use Vision\Vision;
use Vision\Annotation\WebDetection;

class ApiGoogleVisionRequester
{
    /**
     * Get keywords from Google Vision Service
     *
     * @param $file
     * @return array
     */
    public function getKeywordsGoogleVisionLabelDetection($file) : array
    {
        try {

            $clearKeywords = [];

            $vision = new Vision(
                config('app.constants.api_key'),
                [
                    // See a list of all features in the table below
                    // Feature, Limit
                    new Feature(Feature::LABEL_DETECTION, 50),
                ]
            );

            $keyworderService = new KeyworderService();

            $imagePath = public_path(config('app.constants.folder_origins') . $file);
            $response = $vision->request(
            // See a list of all image loaders in the table below
                new LocalImage($imagePath)
            );

            $imageDetects = $response->getLabelAnnotations();

//            $imageDetects = $response->getWebDetection();
//            $detects = $imageDetects->getWebEntities();

            $detects = $imageDetects;

            $keywords = [];
            foreach ($detects as $detect) {
//                $keywords['entity_id'][] = $detect->getEntityId();
                $keywords['entity_id'][] = $detect->getMid();
                $keywords['score'][] = $detect->getScore();
                $keywords['keyword'][] = $detect->getDescription();
            }

            $clearKeywords[] = $keyworderService->clearKeywordsFromExceptions($keywords);

            return $clearKeywords;

        } catch (\Exception $e) {
            dump('Got an error: ' . $e->getMessage());
            return ['error'];
        }
    }

    /**
     * Get keywords from Google Vision Service
     *
     * @param $file
     * @return array
     */
    public function getKeywordsGoogleVisionWebDetection($file) : array
    {
        try {

            $clearKeywords = [];
            
            $vision = new Vision(
                config('app.constants.api_key'),
                [
                    // See a list of all features in the table below
                    // Feature, Limit
                    new Feature(Feature::WEB_DETECTION, 30),
                ]
            );
            
            $keyworderService = new KeyworderService();
           
                $imagePath = public_path(config('app.constants.folder_origins') . $file);
                $response = $vision->request(
                // See a list of all image loaders in the table below
                    new LocalImage($imagePath)
                );

                $imageDetects = $response->getWebDetection();
                $detects = $imageDetects->getWebEntities();

                $keywords = [];
                foreach ($detects as $detect) {
                    $keywords['entity_id'][] = $detect->getEntityId();
                    $keywords['score'][] = $detect->getScore();
                    $keywords['keyword'][] = $detect->getDescription();
                }

                $clearKeywords[] = $keyworderService->clearKeywordsFromExceptions($keywords);

            return $clearKeywords;

        } catch (\Exception $e) {
            dump('Got an error: ' . $e->getMessage());
            return ['error'];
        }
    }
}