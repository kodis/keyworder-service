<?php

/**
 * Service from Microsoft Vision
 */

namespace App\ApiRequesters;

use \HTTP_Request2;
use Illuminate\Support\Arr;

class ApiAzureRequester
{
    /**
     * @param $file
     * @return array|false|string
     */
    public function getDataFromAzureService($file)
    {
        {
            try {

                $apiKey = config('services.azure.api_key');
                $uriBase = config('services.azure.uri_base');
                $imageUrl = config('services.storage.storage_link').$file;

                $captionData = [];

                $request = new HTTP_Request2($uriBase.'/analyze');
                $url = $request->getUrl();

                $headers = array(
                    // Request headers
                    'Content-Type' => 'application/json',
                    'Ocp-Apim-Subscription-Key' => $apiKey,
                );
                $request->setHeader($headers);

                $parameters = array(
                    // Request parameters
                    'visualFeatures' => 'Description',
                    'details' => '',
                    'language' => 'en',
                );
                $url->setQueryVariables($parameters);
                $request->setMethod(HTTP_Request2::METHOD_POST);

                // Request body parameters
                $body = json_encode(array('url' => $imageUrl));

                // Request body
                $request->setBody($body);
                $response = $request->send();
                $data = json_decode($response->getBody(), true);

                $captionData['title'] = Arr::first($data['description']['captions'])['text'];
                $captionData['keywords'] = $data['description']['tags'];

                return $captionData;

            } catch (\Exception $e) {
                dump('azure service error');

                return getExceptionErrors($e);
            }
        }
    }
}