<?php
/**
 * Created by PhpStorm.
 * User: dmitry.korol
 * Date: 06-Nov-19
 * Time: 16:24
 */

namespace App\Services;


use App\ApiRequesters\ApiAzureRequester;
use App\ApiRequesters\ApiClarifaiRequester;
use App\ApiRequesters\ApiCloudsightRequester;
use App\ApiRequesters\ApiShutterstockRequester;
use App\FtpUploaders\FtpStorage;
use Illuminate\Support\Facades\File;

class KeyworderService
{
    public function uploadFilesToFtp() : void
    {
        $uploader = new FtpStorage();
        $uploader->uploadImagesToMyFtp();
    }
    /**
     * @param $file
     * @return array of keywords
     */
    public function getKeywords($file): array
    {
        $apiShutterstockRequester = new ApiShutterstockRequester();

        return $apiShutterstockRequester->leaveOnlyEnglishWordsInKeywords($file);
    }

    /**
     * @param $file
     * @return array
     */
    public function getAdditionalKeywords($file) : array
    {
        $apiClarifaiRequester = new ApiClarifaiRequester();

        return $apiClarifaiRequester->getClarifaiServiceResult($file);
    }

    /**
     * @param $file
     * @return mixed
     */
    public function getAzureKeywords($file) : array
    {
        $apiAzureRequester = new ApiAzureRequester();
        $dataAzure = $apiAzureRequester->getDataFromAzureService($file);

        return $dataAzure['keywords'];
    }

    /**
     * @param $file
     * @return array
     */
    public function getMergedUniqueKeywords($file): array
    {
        $keywords1 = $this->getKeywords($file);
        $keywords2 = $this->getAdditionalKeywords($file);
        $keywords3 = $this->getAzureKeywords($file);

        dump('Keywords Google Vision(1) and Clarifai(2), merged result(3)',[
            'key1' => $keywords1,
            'key2' => $keywords2,
            'key3' => $keywords3,
            'key4' => array_unique(array_merge ($keywords1, $keywords2, $keywords3)),
        ]);

        return array_unique(array_merge ($keywords1, $keywords2, $keywords3));
    }

    /**
     * @param $file
     * @return string
     */
    public function getTitle($file) : string
    {
        $apiCloudsightRequester = new ApiCloudsightRequester();

        return $apiCloudsightRequester->getTitleFromCloudsightService($file);
    }

    /**
     * @param $file
     * @return string
     */
    public function getAdditionalTitle($file) : string
    {
        $apiAzureRequester = new ApiAzureRequester();
        $dataAzure = $apiAzureRequester->getDataFromAzureService($file);

        return $dataAzure['title'];
    }

    /**
     * get title based on shutterstock results
     * @param $file
     * @return string
     */
    public function getShutterstockTitle($file) : string
    {
        $apiShutterstockRequester = new ApiShutterstockRequester();

        return $apiShutterstockRequester->getMostRevalentTitle($file);
    }

    /**
     * copy files in to result folder
     */
    public function makeCopyOfImages(): void
    {
        $files = $this->getFilesNamesInFolder();

        foreach ($files as $file) {
            $imagePath = public_path(config('app.constants.folder_origins').$file);
            copy($imagePath, public_path(config('app.constants.folder_done').$file));
        }
    }

    /**
     * @return array of file's names in source folder
     */
    public function getFilesNamesInFolder(): array
    {
        $path = public_path(config('app.constants.folder_origins'));
        $namesOfFiles = [];
        $filesInFolder = File::files($path);

        foreach ($filesInFolder as $file) {
            $info = pathinfo($file);
            $namesOfFiles[] = $info['basename'];
        }

        return $namesOfFiles;
    }

    /**
     * @param $rec
     * @param $data
     * @param $value
     * @return string
     */
    public function generateImageEntity($rec, $data, $value): string
    {
        try {
            $length = strlen($value);
            $retval = chr(0x1C).chr($rec).chr($data);
            if ($length < 0x8000) {
                $retval .= chr($length >> 8).chr($length & 0xFF);
            } else {
                $retval .= chr(0x80).
                    chr(0x04).
                    chr(($length >> 24) & 0xFF).
                    chr(($length >> 16) & 0xFF).
                    chr(($length >> 8) & 0xFF).
                    chr($length & 0xFF);
            }

            return $retval.$value;
        } catch (\Exception $e) {
            dump('getImageNameInRetval', $e->getMessage());
        }
    }


    /**
     * Return array of random keywords phrases from all shutterstock results
     *
     * @param $keywords
     * @return array
     */
    public function getPhrasesFromShutterResults($keywords): array
    {
        $randomKeys = array_rand($keywords, 3);
        $randomValues = [];

        foreach ($randomKeys as $randomKey) {
            $randomValues[] = $keywords[$randomKey];
        }

        return $randomValues;
    }


    /**
     * Clear keywords from exceptions
     *
     * @param $keywords
     * @return array
     */
    public function clearKeywordsFromExceptions($keywords): array
    {
        $exceptionKeywords = config('app.constants.exceptions');
        $clearestKeywords = [];

        foreach ($keywords['keyword'] as $keyword) {
            if (!in_array($keyword, $exceptionKeywords, false) && !is_null($keyword)) {
                $clearestKeywords[] = $keyword;
            }
        }

        return $clearestKeywords;
    }

    // unused function
    public function generateTitle($file): string
    {

        $data = $this->getRevalentResult($file);
        dd('Data', $data);
        return $data['description'][0];
    }

    // unused function
    /**
     * @param $file
     * @return array
     */
    public function getRevalentResult($file) : array
    {
        try {
            $data = $this->mappingDataFromShutterstock($file);
            dd($data);
            $dataKeywords = $data['keywords'];
            $clearKeywords = $this->getKeywords($file);
            dd('Clear keywords',$clearKeywords);

            $selectedDataKeywords = [];
            foreach ($dataKeywords as $dataKeyword) {
                $nextMatches = next($dataKeywords);

                if ($nextMatches) {
                    $countMatches = count(array_intersect($clearKeywords, $dataKeyword));
                    $countNextMatches = count(array_intersect($clearKeywords, $nextMatches));
                    if ($countNextMatches > $countMatches) {
//                        dump('matched words', array_intersect($clearKeywords, $nextMatches));
                        $selectedDataKeywords = next($dataKeywords);
                        dump('hello',$selectedDataKeywords);
                    }
                }
            }

            // уже есть результат, но очень не точный, поэтому думаю стоит заюзать mykeyworder site.
            // он намного лучше отличает картинки, то есть кидать туда свою картинку, чтобы дополнить ее более
            // точными ключевиками

            dd('$selectedDataKeywords', $selectedDataKeywords);

            return $selectedDataKeywords;
        } catch (\Exception $e) {
            dump('getRevalentResult', $e->getMessage());
        }
    }

    // unused function
    public function mappingDataFromShutterstock($file): array
    {

        $apiShutterstockRequester = new ApiShutterstockRequester();

        $clearKeywords = $this->getKeywords($file);
        $randomPhrases = $this->getPhrasesFromShutterResults($clearKeywords); // array of random phrases
        $shutterResponse = $apiShutterstockRequester->getResultsFromShutterstock($randomPhrases);

        // получил результаты из шаттерстока, их очень много, теперь надо написать функцию, которая пройдется по
        // каждому результату, найдет наибольшее кол-во совпавших ключевиков в каждом запросе и выберет тот тайтл,
        // который будет самым релевантным исходя из ключевиков
        dd('here3');

        $foundTitles = $shutterResponse->data;

        dd('here5', $shutterResponse);

        $resultCollection = [];

        foreach ($foundTitles as $foundTitle) {

            $categories = $foundTitle->categories;
            $categoriesCollection = [];

            foreach ($categories as $category) {
                $categoriesCollection['id'][] = $category->id;
                $categoriesCollection['name'][] = $category->name;
            }

            $resultCollection['id'][] = $foundTitle->id;
            $resultCollection['keywords'][] = $foundTitle->keywords;
            $resultCollection['description'][] = $foundTitle->description;
            $resultCollection['categories'][] = $categoriesCollection;
            $resultCollection['media_type'][] = $foundTitle->media_type;
            $resultCollection['is_illustration'][] = $foundTitle->is_illustration;
            $resultCollection['is_adult'][] = $foundTitle->is_adult;
        }

        dump($resultCollection);

        return $resultCollection;
    }

    // unused function
    public function generateDescription($keywords): string
    {
        // получив ключевые слова, можно делать запрос к api shuterstock и получив
        // результаты можно из наиболее ревалентных составить предложение
        return false;
    }
}